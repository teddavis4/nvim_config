call plug#begin()
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'scrooloose/nerdtree'
Plug 'tmhedberg/SimpylFold'
Plug 'jlanzarotta/bufexplorer'
Plug 'flazz/vim-colorschemes'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'felixhummel/setcolors.vim'
Plug 'majutsushi/tagbar'
Plug 'vim-airline/vim-airline'
Plug 'ryanoasis/vim-devicons'
Plug 'tpope/vim-fugitive'
Plug 'jamessan/vim-gnupg'
Plug 'tpope/vim-obsession'
Plug 'Vimjas/vim-python-pep8-indent'
Plug 'frazrepo/vim-rainbow'
Plug 'junegunn/rainbow_parentheses.vim'
call plug#end()
set nocompatible

highlight Normal guifg=gray guibg=black

set t_Co=256
set updatetime=300
set hidden
colorscheme jellybeans
let g:jellybeans_overrides = {'Todo': { 'guifg': '303030', 'guibg': 'f0f000', 'ctermfg': 'Black', 'ctermbg': 'Yellow', 'attr': 'bold' }}

set colorcolumn=80
highlight ColorColumn ctermbg=green

" Indenting, and style related
set shiftwidth=4
set softtabstop=4
set tabstop=4
set expandtab
" Be smart when using tabs ;)
set smarttab
set foldmethod=syntax
set foldenable
set foldcolumn=1
set foldlevel=0
set foldminlines=5
set foldclose=all
set foldnestmax=10
syntax on
autocmd FileType * set tabstop=4|set shiftwidth=4|set expandtab|set softtabstop=4|set textwidth=169|set colorcolumn=170
autocmd FileType yaml set tabstop=2|set shiftwidth=2|set expandtab|set softtabstop=2|set textwidth=169|set colorcolumn=170
autocmd FileType go set tabstop=4|set shiftwidth=4|set expandtab|set softtabstop=4|set textwidth=199|set colorcolumn=200
autocmd FileType python set tabstop=4|set shiftwidth=4|set expandtab|set softtabstop=4|set textwidth=129|set colorcolumn=130
filetype indent plugin on
" Modeline configs
set modeline
" Append modeline after last line in buffer.
" Use substitute() instead of printf() to handle '%%s' modeline in LaTeX
" files.
function! AppendModeline()
  let l:modeline = printf(" vim: set ts=%d sw=%d tw=%d %set :",
        \ &tabstop, &shiftwidth, &textwidth, &expandtab ? '' : 'no')
  let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
  call append(line("$"), l:modeline)
endfunction
nnoremap <silent> <Leader>ml :call AppendModeline()<CR>
" au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
set encoding=utf-8
set number " makes the current line show the actual number, and surrounding lines have relative numbers

" Turn on mouse support
set mouse=a

" allow vim-go to do things for us
set autowrite

" incremental search -- if you start typing a search phrase it will search as you go
set incsearch

set backup " backup on
set backupdir=~/.backup
set directory=~/.backup

inoremap jk <Esc>

" Ignore case when searching
set ignorecase
" When searching try to be smart about cases
set smartcase

" Show matching brackets when text indicator is over them
set showmatch
" How many tenths of a second to blink when matching brackets
set mat=4


" STATUS BAR RELATED SETTINGS
" CurrSubName gets the current function, class, method, etc. name and puts it
" in the status bar
function! CurrSubName()
    let g:subname = ""
    let g:subrecurssion = 0

    " See if this is a Perl file
    let l:firstline = getline(1)

    if ( l:firstline =~ '#!.*perl' || l:firstline =~ '^package ' )
        call GetSubName(line("."))
    endif

    return g:subname
endfunction

set laststatus=2        " ls:  always put a status line
set statusline=%t[%{strlen(&fenc)?&fenc:'none'},%{&ff}]%h%m%r%y%=%c,%l/%L\ %P
set statusline^=%{coc#status()}

" PLUGIN SETTINGS


" Enable tab completion for asyncomplete.vim
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" inoremap <expr> <cr>    pumvisible() ? "\<C-y>" : "\<cr>"
" show preview window on auto complete
" set completeopt+=preview
set cot+=preview
" close it when auto complete is done
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif

"
"let python_highlight_all=1
let python_highlight_space_errors = 1

" NERDTree
let NERDTreeQuitOnOpen=1
map <C-n> :NERDTreeToggle<CR>
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

map ;w :w!<CR>
map ;d :%!autopep8 --max-line-length=129 -a -a -<CR>
map ;i :!isort -w 129 %<CR>
" map ;s mzgg=G`z;w
map ;s :%!shfmt -i 4 -ci<CR>
map ;y :terminal<CR>

command! BufOnly silent! execute "%bd|e#"

nnoremap <silent> <F9> :TagbarToggle<CR>
let g:airline#extensions#tabline#enabled=1
nnoremap  <silent>   <tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bnext<CR>
nnoremap  <silent> <s-tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bprevious<CR>

map <F1> :BufExplorer<CR>
map <F2> :ls<CR>:b<SPACE>

" COC
" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[c` and `]c` to navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
" autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add diagnostic info for https://github.com/itchyny/lightline.vim
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'cocstatus', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'cocstatus': 'coc#status'
      \ },
      \ }



" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
